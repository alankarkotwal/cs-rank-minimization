Approximations For Solving L0-Norm Minimization Problems In Compressed Sensing
========

Compressed Sensing is a signal processing technique for under-sampling and optimal recovery of a signal, in particular images. It exploits the fact that natural images are sparse in some domain like the Discrete Fourier Transform or Wavelets. The sparsity of the signal can be exploited via optimization to recover it from far fewer samples than required by the Nyquist-Shannon sampling theorem. The sparsity constraint, however, is of a combinatorial nature and is not convex. We explore various methods for solving this problem via convex optimization.

Detailed report now in the repository.